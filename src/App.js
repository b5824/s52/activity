import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import './App.css';
import Home from './Pages/Home';
import Courses from './Pages/Courses';
import Register from './Pages/Register';
import Login from './Pages/Login';
import { loginUser, registerUser } from './data/user';

import { useState } from 'react';

function App() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPass, setConfirmPass] = useState('');
  const [loginEmail, setLoginEmail] = useState('');
  const [loginPassword, setLoginPassword] = useState('');

  registerUser(email, password, confirmPass);

  return (
    <div>
      <AppNavBar />
      <Container>
        <Home />
        <Courses />
        <Register />
        <hr></hr>
        <Login />
      </Container>
    </div>
  );
}

export default App;
