// /*
// NOTE:    How Does React Handle Forms?
// */
// import { Form, Button } from 'react-bootstrap';
// import { useEffect, useState } from 'react';

// export default function Register() {
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');
//   const [confirmPass, setConfirmPass] = useState('');
//   const [isActive, setIsActive] = useState(false);

//   // console.log(email);
//   // console.log(password);
//   // console.log(confirmPass);

//   useEffect(() => {
//     if (
//       email !== '' &&
//       password !== '' &&
//       confirmPass !== '' &&
//       password === confirmPass
//     ) {
//       setIsActive(true);
//     } else {
//       setIsActive(false);
//     }
//   }, [email, password, confirmPass]);

//   const registerUser = (e) => {
//     e.preventDefault();
//     setEmail('');
//     setPassword('');
//     setConfirmPass('');
//   };

//   return (
//     <>
//       <h1>Register Form:</h1>
//       {/* You ALWAYS have to access the event first before calling the function and passing in the event. BEAR IN MIND that you need to first identify where the attributes belongs to and then attach it to its parent. It needs to placed exactly where it is. */}
//       <Form onSubmit={(e) => registerUser(e)}>
//         <Form.Group controlId='userEmail'>
//           <Form.Label>Email address:</Form.Label>
//           <Form.Control
//             type='email'
//             placeholder='Enter your email'
//             required
//             value={email}
//             onChange={(e) => setEmail(e.target.value)}
//           />
//           <Form.Text className='text-muted'>
//             We'll never share your email with anyone.
//           </Form.Text>
//         </Form.Group>

//         <Form.Group controlId='password'>
//           <Form.Label>Password:</Form.Label>
//           <Form.Control
//             type='password'
//             placeholder='Enter your password'
//             required
//             value={password}
//             onChange={(e) => setPassword(e.target.value)}
//           />
//         </Form.Group>

//         <Form.Group controlId='password2'>
//           <Form.Label>Verify Password:</Form.Label>
//           <Form.Control
//             type='password'
//             placeholder='Confirm password'
//             required
//             value={confirmPass}
//             onChange={(e) => setConfirmPass(e.target.value)}
//           />
//         </Form.Group>

//         {/*       {isActive ? (
//           <Button
//             variant='success'
//             type='submit'
//             id='submitBtn'
//             className='mt-3 mb-3'
//           >
//             Submit
//           </Button>
//         ) : (
//           <Button
//             variant='danger'
//             type='submit'
//             id='submitBtn'
//             className='mt-3 mb-3'
//             disabled
//           >
//             Submit
//           </Button>
//         )} */}

//         <Button
//           variant={isActive ? 'success' : 'danger'}
//           type='submit'
//           id='submitBtn'
//           className='mt-3 mb-3'
//           disabled={isActive ? false : true}
//         >
//           Submit
//         </Button>
//       </Form>
//     </>
//   );
// }

import { Form, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { registerUser } from '../data/user';
/* 
NOTE:    How Does React Handle Forms?
*/

export default function Register(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPass, setConfirmPass] = useState('');
  const [isActive, setIsActive] = useState(false);

  // console.log(email);
  // console.log(password);
  // console.log(confirmPass);

  useEffect(() => {
    if (
      email !== '' &&
      password !== '' &&
      confirmPass !== '' &&
      password === confirmPass
    ) {
      registerUser(email, password, confirmPass);
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, confirmPass]);

  const registerUsers = (e) => {
    e.preventDefault();
    // props.email(email);
    // props.password(password);
    // props.confirmPass(confirmPass);

    setEmail('');
    setPassword('');
    setConfirmPass('');
  };

  return (
    <>
      <h1>Register Form:</h1>
      {/* You ALWAYS have to access the event first before calling the function and passing in the event. BEAR IN MIND that you need to first identify where the attributes belongs to and then attach it to its parent. It needs to placed exactly where it is. */}
      <Form onSubmit={(e) => registerUsers(e)}>
        <Form.Group controlId='userEmail'>
          <Form.Label>Email address:</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter your email'
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className='text-muted'>
            We'll never share your email with anyone.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId='password'>
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type='password'
            placeholder='Enter your password'
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId='password2'>
          <Form.Label>Verify Password:</Form.Label>
          <Form.Control
            type='password'
            placeholder='Confirm password'
            required
            value={confirmPass}
            onChange={(e) => setConfirmPass(e.target.value)}
          />
        </Form.Group>

        {/*       {isActive ? (
          <Button
            variant='success'
            type='submit'
            id='submitBtn'
            className='mt-3 mb-3'
          >
            Submit
          </Button>
        ) : (
          <Button
            variant='danger'
            type='submit'
            id='submitBtn'
            className='mt-3 mb-3'
            disabled
          >
            Submit
          </Button>
        )} */}

        <Button
          variant={isActive ? 'success' : 'danger'}
          type='submit'
          id='submitBtn'
          className='mt-3 mb-3'
          disabled={isActive ? false : true}
        >
          Submit
        </Button>
      </Form>
    </>
  );
}
