/*  Activity s52
>> Create Login.js under pages folder
>> The user should input their email and password for logging in
>> When all the input fields are filled, enable the submit button with a different variant color.
    >> The login button should be disabled and has a different variant color before all fields are filled.
>> Upon clicking the submit button, a message will alert the user of a successful login.
>> After clicking the alert, login field should be cleared and will not redirect to any part of the page
>> Render the login page in App.js
>> Send your outputs in Hangouts and link it in Boodle
*/
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { loginUser } from '../data/user';

export default function Login(props) {
  const [isActive, setIsActive] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const checker = (e) => {
    e.preventDefault();

    let test = loginUser(email, password);
    console.log('test ---- ', test);
    isActive && test
      ? alert('You are logged in as ' + email)
      : alert('User not found');
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    }
  }, [email, password]);

  return (
    <>
      <h1>Login</h1>
      <Form onSubmit={(e) => checker(e)}>
        <Form.Group className='mb-3' controlId='formBasicEmail'>
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className='text-muted'>
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        {/* https://upmostly.com/tutorials/react-onchange-events-with-examples */}
        <Form.Group className='mb-3' controlId='formBasicPassword'>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type='password'
            placeholder='Password'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        <Button
          variant={isActive ? 'primary' : 'danger'}
          type='submit'
          disabled={isActive ? false : true}
        >
          Submit
        </Button>
      </Form>
    </>
  );
}
